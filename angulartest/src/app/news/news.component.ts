import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { FormBuilder, Validators } from "@angular/forms";


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  isSubmitted = false;

  articles;
  Country: any = [['Ukraine','ua'],['Russia','ru'],['United States','us']];
  
  constructor(private apiService: ApiService, public fb: FormBuilder) { }
  registrationForm = this.fb.group({
    countryName: ['', [Validators.required]],
    
  });
  
  get countryName() {
    return this.registrationForm.get('countryName');
    
  }

  onSubmit() {
    this.isSubmitted = true;
    if (!this.registrationForm.valid) {
      return false;
    } else {
      console.log(this.registrationForm.value.countryName);
      return this.ngOnInit();
    }

  }
  ngOnInit() {
    this.apiService.getNews(`${this.registrationForm.value.countryName[1]}`).subscribe((data)=>{
      this.articles = data['articles'];
    });
  }

}

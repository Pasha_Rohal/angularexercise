import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  API_KEY = '1157af2693564348b9bc3e19d3218f49';
  
  constructor(private httpClient: HttpClient) { }

  public getNews(value){
    
    return this.httpClient.get(`https://newsapi.org/v2/top-headlines?country=${value}&apiKey=${this.API_KEY}`);
  }
}
